import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println(solveFirstTask(1, 2));
        System.out.println(solveSecondTask(-5, -1));
        solveThirdTask(1, 4, 1);
        solveFourthTask(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
        System.out.println(solveFifthTask(new int[] {11, 12, 3, 14, 5, 6, 18, 7, 9}));
        System.out.println(Arrays.deepToString(solveSixthTask(new double[] {1, 2, 3, 4, 5})));
    }
    public static double solveFirstTask(double x, double y) {
        double sin = Math.pow(Math.sin(x + y), 2);
        double abs = Math.abs(x - (2 * x / (1 + Math.pow(x * y, 2))));
        return ((1 + sin) / (2 + abs)) + x;
    }
    public static boolean solveSecondTask(double x, double y) {
        x = Math.abs(x);
        boolean retValue;
        if (y <= 5 && y >= -3) {
            if (y > 0)
                retValue = x <= 4;
            else retValue = x <= 6;
        } else retValue = false;
        return retValue;
    }
    public static void solveThirdTask(double a, double b, double h) {
        double x = a;
        System.out.println();
        while (x <= b) {
            System.out.printf("%.2f %.2f\n", x, Math.tan(x));
            x += h;
        }
        System.out.println();
    }
    static boolean isPrime(int num) {
        if(num <= 1)
            return false;
        for(int i = 2; i <= num / 2; i++)
            if((num % i) == 0)
                return  false;
        return true;
    }
    public static void solveFourthTask(int[] array) {
        int arrayLen = array.length;
        System.out.println();
        for (int i = 0; i < arrayLen; i++) {
            if (isPrime(array[i]))
                System.out.println(i);
        }
        System.out.println();
    }
    public static int solveFifthTask(int[] array) {
        ArrayList<ArrayList<Integer>> tmpArray = new ArrayList<>();
        int changes;
        int retValue = array.length;
        boolean added;
        for (int arrayElem : array) {
            added = false;
            if (tmpArray.isEmpty()) {
                tmpArray.add(new ArrayList<>());
                tmpArray.get(0).add(arrayElem);
            }
            else {
                for (ArrayList<Integer> intArray : tmpArray) {
                    if (added)
                        break;
                    if (intArray.get(intArray.size() - 1) < arrayElem) {
                        intArray.add(arrayElem);
                        added = true;
                    }
                }
                if (!added) {
                    tmpArray.add(new ArrayList<>());
                    tmpArray.get(tmpArray.size() - 1).add(arrayElem);
                }
            }
        }
        for (ArrayList<Integer> intArray : tmpArray) {
            changes = array.length - intArray.size();
            if (changes < retValue)
                retValue = changes;
        }
        return retValue;
    }
    public static double[][] solveSixthTask(double[] array) {
        double[][] retValue = new double[array.length][array.length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                retValue[i][j] = array[(i + j) % array.length];
            }
        }
        return retValue;
    }
}